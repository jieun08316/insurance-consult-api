package com.ardr.insuranceconsultapi.Repository;

import com.ardr.insuranceconsultapi.entity.InsuranceConsult;
import org.springframework.data.jpa.repository.JpaRepository;


public interface InsuranceConsultRepository extends JpaRepository<InsuranceConsult, Long> {
}
