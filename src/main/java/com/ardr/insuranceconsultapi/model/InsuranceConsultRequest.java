package com.ardr.insuranceconsultapi.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InsuranceConsultRequest {
    private String name;
    private String birth;
    private Boolean isMan;
    private String conTime;
    private String phoneNumber;

}
