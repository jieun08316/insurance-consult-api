package com.ardr.insuranceconsultapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceConsultApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsuranceConsultApiApplication.class, args);
	}

}
